﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HARAnalyzer.Model
{

    public class Creator
    {
        public string name { get; set; }
        public string version { get; set; }
    }

    public class PageTimings
    {
        public double onContentLoad { get; set; }
        public double onLoad { get; set; }
        /// <summary>
        /// V.1.1 firebug
        /// </summary>
        public string comment { get; set; }
        public List<TimeStamps> _timeStamps { get; set; }
        
    }

    public class Page
    {
        public string startedDateTime { get; set; }
        public string id { get; set; }
        public string title { get; set; }
        public PageTimings pageTimings { get; set; }
    }

    public class Request
    {
        public string method { get; set; }
        public string url { get; set; }
        public string httpVersion { get; set; }
        public List<Header> headers { get; set; }
        public List<KeyParType> queryString { get; set; }
        public List<KeyParType> cookies { get; set; }
        public int headersSize { get; set; }
        public int bodySize { get; set; }
        public PostData postData { get; set; }

        public string Name
        {
            get
            {
                string result = string.Empty;
                if (url.Length > 0)
                {
                    result = System.IO.Path.GetFileName(url);
                    if (result.Length == 0)
                        result = "/";
                }
                    
                return result;
            }
        }

        public string Domain
        {
            get
            {
                string result = string.Empty;
                if (url.Length > 0)
                {
                    int a = url.IndexOf("//") + 2;
                    result = url.Substring(0, url.IndexOf("/", a));
                }
                return result;
            }
        }

        public string GetHeadersSource(bool prependHost)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(String.Format("{0} {1}", method, url, httpVersion));
            if (prependHost)
                sb.AppendLine(String.Format("Host: {0}", url.Substring(0, url.IndexOf("/", url.IndexOf("://") + 3))));
            //sb.AppendLine("Connection: keep-alive");
            //sb.AppendLine("Pragma: no-cache");
            //sb.AppendLine("Cache-Control: no-cache");
            //sb.AppendLine("X-Requested-With: XMLHttpRequest");
            //sb.AppendLine("Accept-Encoding: gzip, deflate, sdch, br");
            //sb.AppendLine("Accept-Language: es-ES,es;q=0.8");
            foreach (Header h in headers)
                sb.AppendLine(String.Format("{0}: {1}", h.name, h.value));

            return sb.ToString();
        }

        /// <summary>
        /// v1.2 Chrome Post Content Parsed
        /// </summary>
        /// <returns></returns>
        public string GetPostParsed()
        {
            if (postData == null || postData.text.Length == 0)
                return string.Empty;

            bool isrUrlEncoded = false;
            Header contentType = headers.FirstOrDefault<Header>(h => h.name == "Content-Type");
            isrUrlEncoded = (contentType.value == "application/x-www-form-urlencoded");

            string[] postArray = postData.text.Split(new char[]{'&'});
            StringBuilder result = new StringBuilder();
            foreach (string p in postArray)
            { 
                int a = p.IndexOf("=") + 1;
                string name = p.Substring(0, a);
                string value = p.Substring(a, p.Length - a);
                if (isrUrlEncoded)
                    value = System.Web.HttpUtility.UrlEncode(value);
                result.AppendLine(string.Format("{0}{1}", name, value));
                result.AppendLine("-------------------");
            }
            return result.ToString();
        }

        /// <summary>
        /// v1.1 - Firegub Post Content
        /// </summary>
        /// <returns></returns>
        public string GetQueryStringBase()
        { 
            StringBuilder sb = new StringBuilder();
            foreach(KeyParType q in queryString)
            {
                sb.Append(string.Format("{0}={1}&=", q.name, q.value));
            }
            if (sb.Length > 0)
                return sb.Remove(sb.Length - 2, 2).ToString();
            else
                return string.Empty;
        }

        /// <summary>
        /// v1.1 - Firegub Post Content Parsed
        /// </summary>
        /// <returns></returns>
        public string GetQueryStringParsed()
        { 
            StringBuilder sb = new StringBuilder();
            foreach(KeyParType q in queryString)
            {
                sb.AppendLine(string.Format("{0}={1}&=", q.name, q.value));
                sb.AppendLine("-------------------");
            }
            if (sb.Length > 0)
                return sb.Remove(sb.Length - 2, 2).ToString();
            else
                return string.Empty;
        }
    }

    public class Cooky
    {
        public string name { get; set; }
        public string value { get; set; }
        public string path { get; set; }
        public string expires { get; set; }
        public bool httpOnly { get; set; }
        public bool secure { get; set; }
        public string domain { get; set; }
    }

    public class Content
    {
        public int size { get; set; }
        public string mimeType { get; set; }
        public int compression { get; set; }
        public string text { get; set; }
    }

    public class Response
    {
        public int status { get; set; }
        public string statusText { get; set; }
        public string httpVersion { get; set; }
        public List<Header> headers { get; set; }
        public List<Cooky> cookies { get; set; }
        public Content content { get; set; }
        public string redirectURL { get; set; }
        public int headersSize { get; set; }
        public int bodySize { get; set; }
        public int _transferSize { get; set; }

        public string GetHeadersSource()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(String.Format("{0} {1} {2}", httpVersion, status, statusText));
            foreach (Header h in headers)
                sb.AppendLine(String.Format("{0}: {1}", h.name, h.value));

            return sb.ToString();
        }

    }

    public class Cache
    {
    }

    public class Timings
    {
        public double? blocked { get; set; }
        public double? dns { get; set; }
        public double? connect { get; set; }
        public double? send { get; set; }
        public double? wait { get; set; }
        public double? receive { get; set; }
        public double? ssl { get; set; }
    }

    public class Entry
    {
        public int id { get; set; }
        public string startedDateTime { get; set; }
        public double? time { get; set; }
        public Request request { get; set; }
        public Response response { get; set; }
        public Cache cache { get; set; }
        public Timings timings { get; set; }
        public string serverIPAddress { get; set; }
        public string connection { get; set; }
        public string pageref { get; set; }
        public string name { 
            get
            {
                return request.url;
            }  
        }
    }

    public class Log
    {
        public string version { get; set; }
        public Creator creator { get; set; }
        public List<Page> pages { get; set; }
        public List<Entry> entries { get; set; }
    }

    public class RootObject
    {
        public Log log { get; set; }
    }

    public class PostData
    {
        public string mimeType { get; set; }
        public string text { get; set; }
    }

    /// <summary>
    /// v1.1 Firebug
    /// </summary>
    public class TimeStamps
    {
        public int time { get; set; }
        public string label { get; set; }
    }

    public class Header
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public class KeyParType
    {
        public string name { get; set; }
        public string value { get; set; }
    }
}

