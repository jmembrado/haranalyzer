﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace HARAnalyzer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            UC_HARAnalyzer.ClearTmpFolder();

            Application.Run(new F_Main());
        }
    }
}
