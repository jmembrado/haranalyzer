﻿namespace HARAnalyzer
{
    partial class UC_HARAnalyzer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dGV_Items = new System.Windows.Forms.DataGridView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tSB_Refresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tSTB_DomainFilter = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.tSMI_Text = new System.Windows.Forms.ToolStripMenuItem();
            this.tSMI_CSS = new System.Windows.Forms.ToolStripMenuItem();
            this.tSMI_JS = new System.Windows.Forms.ToolStripMenuItem();
            this.tSMI_Images = new System.Windows.Forms.ToolStripMenuItem();
            this.tSMI_Fonts = new System.Windows.Forms.ToolStripMenuItem();
            this.tSMI_JSON = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tSB_About = new System.Windows.Forms.ToolStripButton();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.tLP_RequestHeader = new System.Windows.Forms.TableLayoutPanel();
            this.txt_RequestHeader = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_RequestPostParsed = new System.Windows.Forms.TextBox();
            this.txt_ResquestPostSource = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.txt_ResponseHeader = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.txt_ResponeData = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.tSB_Preview = new System.Windows.Forms.ToolStripButton();
            this.tSB_OpenBrowser = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dGV_Items)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.tLP_RequestHeader.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dGV_Items);
            this.splitContainer1.Panel1.Controls.Add(this.toolStrip1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(751, 513);
            this.splitContainer1.SplitterDistance = 359;
            this.splitContainer1.TabIndex = 3;
            // 
            // dGV_Items
            // 
            this.dGV_Items.AllowUserToAddRows = false;
            this.dGV_Items.AllowUserToDeleteRows = false;
            this.dGV_Items.AllowUserToOrderColumns = true;
            this.dGV_Items.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGV_Items.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dGV_Items.Location = new System.Drawing.Point(0, 25);
            this.dGV_Items.MultiSelect = false;
            this.dGV_Items.Name = "dGV_Items";
            this.dGV_Items.ReadOnly = true;
            this.dGV_Items.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dGV_Items.Size = new System.Drawing.Size(359, 488);
            this.dGV_Items.TabIndex = 2;
            this.dGV_Items.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dGV_Items_CellFormatting);
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripSeparator1,
            this.tSB_Refresh,
            this.toolStripSeparator2,
            this.toolStripLabel2,
            this.toolStripLabel1,
            this.tSTB_DomainFilter,
            this.toolStripDropDownButton1,
            this.toolStripSeparator3,
            this.tSB_About});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(359, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "tS_Items";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::HARAnalyzer.Properties.Resources.folder_16;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(72, 22);
            this.toolStripButton1.Text = "Open File";
            this.toolStripButton1.Click += new System.EventHandler(this.tSB_OpenFile_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tSB_Refresh
            // 
            this.tSB_Refresh.Image = global::HARAnalyzer.Properties.Resources.refresh_16;
            this.tSB_Refresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSB_Refresh.Name = "tSB_Refresh";
            this.tSB_Refresh.Size = new System.Drawing.Size(65, 22);
            this.tSB_Refresh.Text = "Refresh";
            this.tSB_Refresh.Click += new System.EventHandler(this.tSB_Refresh_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Image = global::HARAnalyzer.Properties.Resources.filter_16;
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(56, 22);
            this.toolStripLabel2.Text = "Filters:";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(46, 22);
            this.toolStripLabel1.Text = "Domain:";
            // 
            // tSTB_DomainFilter
            // 
            this.tSTB_DomainFilter.Name = "tSTB_DomainFilter";
            this.tSTB_DomainFilter.Size = new System.Drawing.Size(100, 21);
            this.tSTB_DomainFilter.TextChanged += new System.EventHandler(this.tSB_Refresh_Click);
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tSMI_Text,
            this.tSMI_CSS,
            this.tSMI_JS,
            this.tSMI_Images,
            this.tSMI_Fonts,
            this.tSMI_JSON});
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(13, 4);
            this.toolStripDropDownButton1.Text = "toolStripDropDownButton1";
            // 
            // tSMI_Text
            // 
            this.tSMI_Text.Checked = true;
            this.tSMI_Text.CheckOnClick = true;
            this.tSMI_Text.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tSMI_Text.Name = "tSMI_Text";
            this.tSMI_Text.Size = new System.Drawing.Size(135, 22);
            this.tSMI_Text.Text = "TEXT";
            this.tSMI_Text.Click += new System.EventHandler(this.tSB_Refresh_Click);
            // 
            // tSMI_CSS
            // 
            this.tSMI_CSS.CheckOnClick = true;
            this.tSMI_CSS.Name = "tSMI_CSS";
            this.tSMI_CSS.Size = new System.Drawing.Size(135, 22);
            this.tSMI_CSS.Text = "CSS";
            this.tSMI_CSS.Click += new System.EventHandler(this.tSB_Refresh_Click);
            // 
            // tSMI_JS
            // 
            this.tSMI_JS.CheckOnClick = true;
            this.tSMI_JS.Name = "tSMI_JS";
            this.tSMI_JS.Size = new System.Drawing.Size(135, 22);
            this.tSMI_JS.Text = "JAVASCRIPT";
            this.tSMI_JS.Click += new System.EventHandler(this.tSB_Refresh_Click);
            // 
            // tSMI_Images
            // 
            this.tSMI_Images.CheckOnClick = true;
            this.tSMI_Images.Name = "tSMI_Images";
            this.tSMI_Images.Size = new System.Drawing.Size(135, 22);
            this.tSMI_Images.Text = "IMAGES";
            this.tSMI_Images.Click += new System.EventHandler(this.tSB_Refresh_Click);
            // 
            // tSMI_Fonts
            // 
            this.tSMI_Fonts.CheckOnClick = true;
            this.tSMI_Fonts.Name = "tSMI_Fonts";
            this.tSMI_Fonts.Size = new System.Drawing.Size(135, 22);
            this.tSMI_Fonts.Text = "FONTS";
            this.tSMI_Fonts.Click += new System.EventHandler(this.tSB_Refresh_Click);
            // 
            // tSMI_JSON
            // 
            this.tSMI_JSON.CheckOnClick = true;
            this.tSMI_JSON.Name = "tSMI_JSON";
            this.tSMI_JSON.Size = new System.Drawing.Size(135, 22);
            this.tSMI_JSON.Text = "JSON";
            this.tSMI_JSON.Click += new System.EventHandler(this.tSB_Refresh_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 6);
            // 
            // tSB_About
            // 
            this.tSB_About.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tSB_About.Image = global::HARAnalyzer.Properties.Resources.help_16;
            this.tSB_About.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSB_About.Name = "tSB_About";
            this.tSB_About.Size = new System.Drawing.Size(23, 20);
            this.tSB_About.Text = "About";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer3);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer4);
            this.splitContainer2.Size = new System.Drawing.Size(388, 513);
            this.splitContainer2.SplitterDistance = 190;
            this.splitContainer2.TabIndex = 0;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.tLP_RequestHeader);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer3.Size = new System.Drawing.Size(190, 513);
            this.splitContainer3.SplitterDistance = 227;
            this.splitContainer3.TabIndex = 0;
            // 
            // tLP_RequestHeader
            // 
            this.tLP_RequestHeader.ColumnCount = 1;
            this.tLP_RequestHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tLP_RequestHeader.Controls.Add(this.txt_RequestHeader, 0, 1);
            this.tLP_RequestHeader.Controls.Add(this.label1, 0, 0);
            this.tLP_RequestHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tLP_RequestHeader.Location = new System.Drawing.Point(0, 0);
            this.tLP_RequestHeader.Name = "tLP_RequestHeader";
            this.tLP_RequestHeader.RowCount = 2;
            this.tLP_RequestHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tLP_RequestHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tLP_RequestHeader.Size = new System.Drawing.Size(190, 227);
            this.tLP_RequestHeader.TabIndex = 0;
            // 
            // txt_RequestHeader
            // 
            this.txt_RequestHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_RequestHeader.Location = new System.Drawing.Point(3, 18);
            this.txt_RequestHeader.Multiline = true;
            this.txt_RequestHeader.Name = "txt_RequestHeader";
            this.txt_RequestHeader.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt_RequestHeader.Size = new System.Drawing.Size(184, 206);
            this.txt_RequestHeader.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Request Header:";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txt_RequestPostParsed, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txt_ResquestPostSource, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label6, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(190, 282);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Plain:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label4, 2);
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Request Content:";
            // 
            // txt_RequestPostParsed
            // 
            this.txt_RequestPostParsed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_RequestPostParsed.Location = new System.Drawing.Point(98, 43);
            this.txt_RequestPostParsed.Multiline = true;
            this.txt_RequestPostParsed.Name = "txt_RequestPostParsed";
            this.txt_RequestPostParsed.Size = new System.Drawing.Size(89, 216);
            this.txt_RequestPostParsed.TabIndex = 1;
            // 
            // txt_ResquestPostSource
            // 
            this.txt_ResquestPostSource.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_ResquestPostSource.Location = new System.Drawing.Point(3, 43);
            this.txt_ResquestPostSource.Multiline = true;
            this.txt_ResquestPostSource.Name = "txt_ResquestPostSource";
            this.txt_ResquestPostSource.Size = new System.Drawing.Size(89, 216);
            this.txt_ResquestPostSource.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(98, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Parsed:";
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.tableLayoutPanel2);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.tableLayoutPanel3);
            this.splitContainer4.Size = new System.Drawing.Size(194, 513);
            this.splitContainer4.SplitterDistance = 226;
            this.splitContainer4.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.txt_ResponseHeader, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(194, 226);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // txt_ResponseHeader
            // 
            this.txt_ResponseHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_ResponseHeader.Location = new System.Drawing.Point(3, 18);
            this.txt_ResponseHeader.Multiline = true;
            this.txt_ResponseHeader.Name = "txt_ResponseHeader";
            this.txt_ResponseHeader.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt_ResponseHeader.Size = new System.Drawing.Size(188, 205);
            this.txt_ResponseHeader.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Response Header:";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.txt_ResponeData, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(194, 283);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // txt_ResponeData
            // 
            this.txt_ResponeData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_ResponeData.Location = new System.Drawing.Point(3, 47);
            this.txt_ResponeData.Multiline = true;
            this.txt_ResponeData.Name = "txt_ResponeData";
            this.txt_ResponeData.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt_ResponeData.Size = new System.Drawing.Size(188, 233);
            this.txt_ResponeData.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.label3);
            this.flowLayoutPanel1.Controls.Add(this.toolStrip2);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(188, 38);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Response Content:";
            // 
            // toolStrip2
            // 
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tSB_Preview,
            this.tSB_OpenBrowser});
            this.toolStrip2.Location = new System.Drawing.Point(0, 13);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(144, 25);
            this.toolStrip2.TabIndex = 1;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // tSB_Preview
            // 
            this.tSB_Preview.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tSB_Preview.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSB_Preview.Name = "tSB_Preview";
            this.tSB_Preview.Size = new System.Drawing.Size(49, 22);
            this.tSB_Preview.Text = "Preview";
            this.tSB_Preview.Click += new System.EventHandler(this.tSB_Preview_Click);
            // 
            // tSB_OpenBrowser
            // 
            this.tSB_OpenBrowser.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tSB_OpenBrowser.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tSB_OpenBrowser.Name = "tSB_OpenBrowser";
            this.tSB_OpenBrowser.Size = new System.Drawing.Size(92, 22);
            this.tSB_OpenBrowser.Text = "Open In Browser";
            this.tSB_OpenBrowser.Click += new System.EventHandler(this.tSB_OpenBrowser_Click);
            // 
            // UC_HARAnalyzer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "UC_HARAnalyzer";
            this.Size = new System.Drawing.Size(751, 513);
            this.Load += new System.EventHandler(this.F_HARAnalyzer_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dGV_Items)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.tLP_RequestHeader.ResumeLayout(false);
            this.tLP_RequestHeader.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton tSB_Refresh;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.TextBox txt_RequestHeader;
        private System.Windows.Forms.TextBox txt_ResponeData;
        private System.Windows.Forms.TextBox txt_RequestPostParsed;
        private System.Windows.Forms.TextBox txt_ResquestPostSource;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem tSMI_Text;
        private System.Windows.Forms.ToolStripMenuItem tSMI_CSS;
        private System.Windows.Forms.ToolStripMenuItem tSMI_JS;
        private System.Windows.Forms.ToolStripMenuItem tSMI_Images;
        private System.Windows.Forms.ToolStripMenuItem tSMI_Fonts;
        private System.Windows.Forms.ToolStripMenuItem tSMI_JSON;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox tSTB_DomainFilter;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.DataGridView dGV_Items;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton tSB_About;
        private System.Windows.Forms.TableLayoutPanel tLP_RequestHeader;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox txt_ResponseHeader;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton tSB_Preview;
        private System.Windows.Forms.ToolStripButton tSB_OpenBrowser;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;

    }
}