﻿using HARAnalyzer.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace HARAnalyzer
{
    public partial class UC_HARAnalyzer : UserControl
    {

        RootObject root;
        Entry currentEntry;

        public UC_HARAnalyzer()
        {
            InitializeComponent();
            init_dGV_Items();
        }

        private void tSB_OpenFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "HAR Chrome v1.2 (*.har)|*.har|HAR Firebug v1.1 (*.fhar)|*.fhar";
            ofd.Title = "Select HAR (HTTP Archives)";
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                StreamReader reader = new StreamReader(ofd.FileName);
                using (reader)
                    root = JsonConvert.DeserializeObject<RootObject>(reader.ReadToEnd());

                for (Int32 i = 0; i < root.log.entries.Count; i++)
                    root.log.entries[i].id = i;

                this.tSB_Refresh_Click(sender, e);
                this.Parent.Text = Path.GetFileNameWithoutExtension(ofd.FileName);
            }
        }

        private void tSB_Refresh_Click(object sender, EventArgs e)
        {
            this.dGV_Items.SelectionChanged -= dGV_Items_SelectionChanged;
            
            List<Entry> entries = new List<Entry>();
            foreach (Entry x in root.log.entries)
            {
                //if (this.tSB_CacheFilter.Checked)
                //    if (x.response._transferSize <= 0)
                //        continue;

                Boolean remove = false;
                switch (x.response.content.mimeType)
                { 
                    case "text/plain":
                    case "text/html":
                        remove = (!this.tSMI_Text.Checked);
                        break;
                    case "text/css":
                        remove = (!this.tSMI_CSS.Checked);
                        break;
                    case "application/javascript":
                        remove = (!this.tSMI_JS.Checked);
                        break;
                    case "application/json":
                        remove = (!this.tSMI_JSON.Checked);
                        break;
                    default:
                        if (x.response.content.mimeType.StartsWith("image"))
                            remove = (!this.tSMI_Images.Checked);
                        break;
                }

                if (!remove && this.tSTB_DomainFilter.Text.Length > 0)
                    remove = !(x.request.url.StartsWith(this.tSTB_DomainFilter.Text));

                if (!remove)
                    entries.Add(x);
            }

            var bindingList = new BindingList<Entry>(entries);
            var source = new BindingSource(bindingList, null);
            this.dGV_Items.DataSource = source;

            this.dGV_Items.SelectionChanged += dGV_Items_SelectionChanged;
            dGV_Items.Columns[1].DataPropertyName = "method";
        }

        private void dGV_Items_SelectionChanged(object sender, EventArgs e)
        {
            currentEntry = root.log.entries.Where(x => x.id == (int)this.dGV_Items.CurrentRow.Cells[0].Value).SingleOrDefault<Entry>();

            this.txt_RequestHeader.Text = currentEntry.request.GetHeadersSource(!(this.root.log.creator.name == "Firebug"));
            this.txt_ResponeData.Text = currentEntry.request.GetPostParsed();
            if (currentEntry.request.postData != null)
            {
                this.txt_ResquestPostSource.Text = currentEntry.request.postData.text;
                this.txt_RequestPostParsed.Text = currentEntry.request.GetPostParsed();
            }
            else if (currentEntry.request.queryString != null && currentEntry.request.queryString.Count > 0)
            {
                this.txt_ResquestPostSource.Text = currentEntry.request.GetQueryStringBase();
                this.txt_RequestPostParsed.Text = currentEntry.request.GetQueryStringParsed();
            }
            else
            {
                this.txt_ResquestPostSource.Text = string.Empty;
                this.txt_RequestPostParsed.Text = string.Empty;
            }

            this.txt_ResponseHeader.Text = currentEntry.response.GetHeadersSource();
            this.txt_ResponeData.Text = currentEntry.response.content.text;
        }

        private void tSB_CacheFilter_Click(object sender, EventArgs e)
        {
            this.tSB_Refresh_Click(sender, e);
        }

        private void F_HARAnalyzer_Load(object sender, EventArgs e)
        {
            this.Text = "HAR Analyzer v" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

            this.tSB_OpenFile_Click(sender, e);

            
        }

        private void tSB_Preview_Click(object sender, EventArgs e)
        {
            if (this.txt_ResponeData.Text.Length > 0)
            {
                F_ContentViewer view = new F_ContentViewer(this.txt_ResponeData.Text);
                view.Show(this);
            }
        }

        private void tSB_OpenBrowser_Click(object sender, EventArgs e)
        {
            string fileName = tempFolder + "\\" + Path.GetFileNameWithoutExtension(Path.GetTempFileName()) + ".html";
            using (StreamWriter sw = new StreamWriter(fileName))
            {
                sw.WriteLine(this.txt_ResponeData.Text);
            }
            Process p = System.Diagnostics.Process.Start(fileName);
        }

        private static string tempFolder = Directory.GetCurrentDirectory() + "\\tmp";
        public static void ClearTmpFolder()
        { 
            if (!System.IO.Directory.Exists(tempFolder))
            {
                System.IO.Directory.CreateDirectory(tempFolder);
            }
            else
            {
                foreach (string f in System.IO.Directory.GetFiles(tempFolder))
                {
                    try
                    {
                        System.IO.Directory.Delete(f);
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }

        private void init_dGV_Items()
        {
            ((System.ComponentModel.ISupportInitialize)(this.dGV_Items)).BeginInit();
            this.dGV_Items.AutoGenerateColumns = false;

            System.Windows.Forms.DataGridViewTextBoxColumn dgvc_Id;
            System.Windows.Forms.DataGridViewTextBoxColumn dGVC_Method;
            System.Windows.Forms.DataGridViewTextBoxColumn dGVC_Domain;
            System.Windows.Forms.DataGridViewTextBoxColumn dGVC_Name;
            System.Windows.Forms.DataGridViewTextBoxColumn dGVC_Status;
            System.Windows.Forms.DataGridViewTextBoxColumn dGVC_Type;
            System.Windows.Forms.DataGridViewTextBoxColumn dGVC_StartTime;
            System.Windows.Forms.DataGridViewTextBoxColumn dGVC_Size;

            dgvc_Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            dGVC_Method = new System.Windows.Forms.DataGridViewTextBoxColumn();
            dGVC_Domain = new System.Windows.Forms.DataGridViewTextBoxColumn();
            dGVC_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            dGVC_Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            dGVC_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            dGVC_StartTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            dGVC_Size = new System.Windows.Forms.DataGridViewTextBoxColumn();

            dGV_Items.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            dgvc_Id,
            dGVC_Domain,
            dGVC_Method,
            dGVC_Name,
            dGVC_Status,
            dGVC_Type,
            dGVC_StartTime,
            dGVC_Size});

            // 
            // dgvc_Id
            // 
            dgvc_Id.HeaderText = "Id";
            dgvc_Id.Name = "dgvc_Id";
            dgvc_Id.ReadOnly = true;
            dgvc_Id.Visible = false;
            dgvc_Id.DataPropertyName = "id";
            // 
            // dGVC_Method
            // 
            dGVC_Method.HeaderText = "Method";
            dGVC_Method.Name = "dGVC_Method";
            dGVC_Method.ReadOnly = true;
            dGVC_Method.DataPropertyName = "request.method";
            // 
            // dGVC_Domain
            // 
            dGVC_Method.HeaderText = "Domain";
            dGVC_Method.Name = "dGVC_Domain";
            dGVC_Method.ReadOnly = true;
            dGVC_Method.DataPropertyName = "request.Domain";
            // 
            // dGVC_Url
            // 
            dGVC_Name.HeaderText = "Name";
            dGVC_Name.Name = "dGVC_Name";
            dGVC_Name.ReadOnly = true;
            dGVC_Name.DataPropertyName = "request.Name";
            // 
            // dGVC_Status
            // 
            dGVC_Status.HeaderText = "Status";
            dGVC_Status.Name = "dGVC_Status";
            dGVC_Status.ReadOnly = true;
            dGVC_Status.DataPropertyName = "response.statusText";
            // 
            // dGVC_Type
            // 
            dGVC_Type.HeaderText = "Type";
            dGVC_Type.Name = "dGVC_Type";
            dGVC_Type.ReadOnly = true;
            dGVC_Type.DataPropertyName = "response.statusText";
            // 
            // dGVC_StartTime
            // 
            dGVC_StartTime.HeaderText = "Start";
            dGVC_StartTime.Name = "dGVC_StartTime";
            dGVC_StartTime.ReadOnly = true;
            dGVC_StartTime.DataPropertyName = "startedDateTime";
            // 
            // dGVC_Size
            // 
            dGVC_Size.HeaderText = "Size";
            dGVC_Size.Name = "dGVC_Size";
            dGVC_Size.ReadOnly = true;

            ((System.ComponentModel.ISupportInitialize)(this.dGV_Items)).EndInit();
        }

        private void dGV_Items_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridViewColumn column = dGV_Items.Columns[e.ColumnIndex];
            if (column.DataPropertyName.Contains("."))
            {
                object data = dGV_Items.Rows[e.RowIndex].DataBoundItem;
                string[] properties = column.DataPropertyName.Split('.');
                for (int i = 0; i < properties.Length && data != null; i++)
                    data = data.GetType().GetProperty(properties[i]).GetValue(data);
                dGV_Items.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = data;
            }
        }
    }
}
