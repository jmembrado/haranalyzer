﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HARAnalyzer
{
    public partial class F_Main : Form
    {
        public F_Main()
        {
            InitializeComponent();

            this.tabControl1.DrawMode = TabDrawMode.OwnerDrawFixed;
            this.tabControl1.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.tabControl1_DrawItem);
        }

        private Rectangle tabArea;
        private RectangleF tabTextArea;

        private void F_Main_Load(object sender, EventArgs e)
        {
            this.AddNewTab();

            tabArea = tabControl1.GetTabRect(0);
            tabTextArea = (RectangleF)tabControl1.GetTabRect(0);
        }

        private void AddNewTab()
        {
            tabControl1.Selecting -= tabControl1_Selecting;
            
            UC_HARAnalyzer ha = new UC_HARAnalyzer();

            TabPage tp = new TabPage("No File Loaded.");
            tp.Controls.Add(ha);
            ha.Dock = DockStyle.Fill;

            this.tabControl1.TabPages.Insert(this.tabControl1.TabPages.Count - 1, tp);

            this.tabControl1.SelectedIndex = tabControl1.TabPages.Count - 2;

            tabControl1.Selecting += tabControl1_Selecting;
        }

        private void tabControl1_DrawItem(object sender, DrawItemEventArgs e)
        {
            Graphics g = e.Graphics;
            Pen p = new Pen(Color.Blue);
            Font font = new Font("Arial", 10.0f);
            SolidBrush brush = new SolidBrush(Color.Red);

            //g.DrawRectangle(p, tabArea);

            if (e.Index != this.tabControl1.TabPages.Count - 1)
                e.Graphics.DrawString("x", e.Font, Brushes.Red, e.Bounds.Right - 15, e.Bounds.Top + 4);

            e.Graphics.DrawString(this.tabControl1.TabPages[e.Index].Text, e.Font, Brushes.Black, e.Bounds.Left, e.Bounds.Top + 4);
            e.DrawFocusRectangle();

        }

        private void tabControl1_MouseDown(object sender, MouseEventArgs e)
        {
            for (int i = 0; i < this.tabControl1.TabPages.Count; i++)
            {
                Rectangle r = tabControl1.GetTabRect(i);
                //Getting the position of the "x" mark.
                Rectangle closeButton = new Rectangle(r.Right - 15, r.Top + 4, 9, 7);
                if (closeButton.Contains(e.Location))
                {
                    if (MessageBox.Show("Would you like to Close this Tab?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        this.tabControl1.TabPages.RemoveAt(i);
                        break;
                    }
                }
            }
        }

        private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (e.TabPageIndex == tabControl1.TabPages.Count - 1)
            {
                this.AddNewTab();
            }
        }
    }
}
